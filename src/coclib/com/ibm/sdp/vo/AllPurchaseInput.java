
package coclib.com.ibm.sdp.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AllPurchaseInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AllPurchaseInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="usageDataRecord" type="{http://CocLib/com/ibm/sdp/vo}UsageDataRecord" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AllPurchaseInput", propOrder = {
    "usageDataRecord"
})
public class AllPurchaseInput {

    protected UsageDataRecord usageDataRecord;

    /**
     * Gets the value of the usageDataRecord property.
     * 
     * @return
     *     possible object is
     *     {@link UsageDataRecord }
     *     
     */
    public UsageDataRecord getUsageDataRecord() {
        return usageDataRecord;
    }

    /**
     * Sets the value of the usageDataRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link UsageDataRecord }
     *     
     */
    public void setUsageDataRecord(UsageDataRecord value) {
        this.usageDataRecord = value;
    }

}
