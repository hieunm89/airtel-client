
package coclib.com.ibm.sdp.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CPServiceDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CPServiceDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="eventType" type="{http://CocLib/com/ibm/sdp/vo}EnumValues" minOccurs="0"/>
 *         &lt;element name="pricePoints" type="{http://CocLib/com/ibm/sdp/vo}PriceAndMessaging" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CPServiceDetails", propOrder = {
    "eventType",
    "pricePoints",
    "id"
})
public class CPServiceDetails {

    protected EnumValues eventType;
    protected List<PriceAndMessaging> pricePoints;
    protected Integer id;

    /**
     * Gets the value of the eventType property.
     * 
     * @return
     *     possible object is
     *     {@link EnumValues }
     *     
     */
    public EnumValues getEventType() {
        return eventType;
    }

    /**
     * Sets the value of the eventType property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumValues }
     *     
     */
    public void setEventType(EnumValues value) {
        this.eventType = value;
    }

    /**
     * Gets the value of the pricePoints property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pricePoints property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPricePoints().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PriceAndMessaging }
     * 
     * 
     */
    public List<PriceAndMessaging> getPricePoints() {
        if (pricePoints == null) {
            pricePoints = new ArrayList<PriceAndMessaging>();
        }
        return this.pricePoints;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setId(Integer value) {
        this.id = value;
    }

}
