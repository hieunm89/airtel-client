
package coclib.com.ibm.sdp.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CDRConfiguration complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CDRConfiguration">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="systemId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="systemName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="colSeprator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rowSeprator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exportTimeGapInHours" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numberOfRecordsPerFile" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cdrFileNamePrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cdrFileNamePostFix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cdrFileSize" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="systemCategory" type="{http://CocLib/com/ibm/sdp/vo}EnumValues" minOccurs="0"/>
 *         &lt;element name="cdrFileFormat" type="{http://CocLib/com/ibm/sdp/vo}EnumValues" minOccurs="0"/>
 *         &lt;element name="application" type="{http://CocLib/com/ibm/sdp/vo}ApplicationDetails" minOccurs="0"/>
 *         &lt;element name="cdrAttributeList" type="{http://CocLib/com/ibm/sdp/vo}CDRAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CDRConfiguration", propOrder = {
    "systemId",
    "systemName",
    "colSeprator",
    "rowSeprator",
    "exportTimeGapInHours",
    "numberOfRecordsPerFile",
    "cdrFileNamePrefix",
    "cdrFileNamePostFix",
    "cdrFileSize",
    "systemCategory",
    "cdrFileFormat",
    "application",
    "cdrAttributeList"
})
public class CDRConfiguration {

    protected Integer systemId;
    protected String systemName;
    protected String colSeprator;
    protected String rowSeprator;
    protected Integer exportTimeGapInHours;
    protected Integer numberOfRecordsPerFile;
    protected String cdrFileNamePrefix;
    protected String cdrFileNamePostFix;
    protected Integer cdrFileSize;
    protected EnumValues systemCategory;
    protected EnumValues cdrFileFormat;
    protected ApplicationDetails application;
    protected List<CDRAttribute> cdrAttributeList;

    /**
     * Gets the value of the systemId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSystemId() {
        return systemId;
    }

    /**
     * Sets the value of the systemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSystemId(Integer value) {
        this.systemId = value;
    }

    /**
     * Gets the value of the systemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemName() {
        return systemName;
    }

    /**
     * Sets the value of the systemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemName(String value) {
        this.systemName = value;
    }

    /**
     * Gets the value of the colSeprator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColSeprator() {
        return colSeprator;
    }

    /**
     * Sets the value of the colSeprator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColSeprator(String value) {
        this.colSeprator = value;
    }

    /**
     * Gets the value of the rowSeprator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRowSeprator() {
        return rowSeprator;
    }

    /**
     * Sets the value of the rowSeprator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRowSeprator(String value) {
        this.rowSeprator = value;
    }

    /**
     * Gets the value of the exportTimeGapInHours property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExportTimeGapInHours() {
        return exportTimeGapInHours;
    }

    /**
     * Sets the value of the exportTimeGapInHours property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExportTimeGapInHours(Integer value) {
        this.exportTimeGapInHours = value;
    }

    /**
     * Gets the value of the numberOfRecordsPerFile property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfRecordsPerFile() {
        return numberOfRecordsPerFile;
    }

    /**
     * Sets the value of the numberOfRecordsPerFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfRecordsPerFile(Integer value) {
        this.numberOfRecordsPerFile = value;
    }

    /**
     * Gets the value of the cdrFileNamePrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdrFileNamePrefix() {
        return cdrFileNamePrefix;
    }

    /**
     * Sets the value of the cdrFileNamePrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdrFileNamePrefix(String value) {
        this.cdrFileNamePrefix = value;
    }

    /**
     * Gets the value of the cdrFileNamePostFix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdrFileNamePostFix() {
        return cdrFileNamePostFix;
    }

    /**
     * Sets the value of the cdrFileNamePostFix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdrFileNamePostFix(String value) {
        this.cdrFileNamePostFix = value;
    }

    /**
     * Gets the value of the cdrFileSize property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCdrFileSize() {
        return cdrFileSize;
    }

    /**
     * Sets the value of the cdrFileSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCdrFileSize(Integer value) {
        this.cdrFileSize = value;
    }

    /**
     * Gets the value of the systemCategory property.
     * 
     * @return
     *     possible object is
     *     {@link EnumValues }
     *     
     */
    public EnumValues getSystemCategory() {
        return systemCategory;
    }

    /**
     * Sets the value of the systemCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumValues }
     *     
     */
    public void setSystemCategory(EnumValues value) {
        this.systemCategory = value;
    }

    /**
     * Gets the value of the cdrFileFormat property.
     * 
     * @return
     *     possible object is
     *     {@link EnumValues }
     *     
     */
    public EnumValues getCdrFileFormat() {
        return cdrFileFormat;
    }

    /**
     * Sets the value of the cdrFileFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumValues }
     *     
     */
    public void setCdrFileFormat(EnumValues value) {
        this.cdrFileFormat = value;
    }

    /**
     * Gets the value of the application property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationDetails }
     *     
     */
    public ApplicationDetails getApplication() {
        return application;
    }

    /**
     * Sets the value of the application property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationDetails }
     *     
     */
    public void setApplication(ApplicationDetails value) {
        this.application = value;
    }

    /**
     * Gets the value of the cdrAttributeList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cdrAttributeList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCdrAttributeList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CDRAttribute }
     * 
     * 
     */
    public List<CDRAttribute> getCdrAttributeList() {
        if (cdrAttributeList == null) {
            cdrAttributeList = new ArrayList<CDRAttribute>();
        }
        return this.cdrAttributeList;
    }

}
