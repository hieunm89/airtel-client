
package coclib.com.ibm.sdp.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PriceAndMessaging complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PriceAndMessaging">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pricePoint" type="{http://CocLib/com/ibm/sdp/vo}PricePoint" minOccurs="0"/>
 *         &lt;element name="messagingAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="partialBillingAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceAndMessaging", propOrder = {
    "pricePoint",
    "messagingAllowed",
    "partialBillingAllowed"
})
public class PriceAndMessaging {

    protected PricePoint pricePoint;
    protected String messagingAllowed;
    protected String partialBillingAllowed;

    /**
     * Gets the value of the pricePoint property.
     * 
     * @return
     *     possible object is
     *     {@link PricePoint }
     *     
     */
    public PricePoint getPricePoint() {
        return pricePoint;
    }

    /**
     * Sets the value of the pricePoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricePoint }
     *     
     */
    public void setPricePoint(PricePoint value) {
        this.pricePoint = value;
    }

    /**
     * Gets the value of the messagingAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessagingAllowed() {
        return messagingAllowed;
    }

    /**
     * Sets the value of the messagingAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessagingAllowed(String value) {
        this.messagingAllowed = value;
    }

    /**
     * Gets the value of the partialBillingAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartialBillingAllowed() {
        return partialBillingAllowed;
    }

    /**
     * Sets the value of the partialBillingAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartialBillingAllowed(String value) {
        this.partialBillingAllowed = value;
    }

}
