
package coclib.com.ibm.sdp.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AttributeDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AttributeDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="attributeId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="attributeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="attributeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="attributeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="attributeLength" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="attributeDefaultValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="attributeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isMandatory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="attributeEnums" type="{http://CocLib/com/ibm/sdp/vo}EnumType" minOccurs="0"/>
 *         &lt;element name="attributeStatus" type="{http://CocLib/com/ibm/sdp/vo}EnumValues" minOccurs="0"/>
 *         &lt;element name="attributeValueSource" type="{http://CocLib/com/ibm/sdp/vo}EnumValues" minOccurs="0"/>
 *         &lt;element name="attributeValueKey" type="{http://CocLib/com/ibm/sdp/vo}EnumValues" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributeDetails", propOrder = {
    "attributeId",
    "attributeName",
    "attributeCode",
    "attributeType",
    "attributeLength",
    "attributeDefaultValue",
    "attributeDescription",
    "isMandatory",
    "attributeEnums",
    "attributeStatus",
    "attributeValueSource",
    "attributeValueKey"
})
public class AttributeDetails {

    protected Integer attributeId;
    protected String attributeName;
    protected String attributeCode;
    protected String attributeType;
    protected Integer attributeLength;
    protected String attributeDefaultValue;
    protected String attributeDescription;
    protected Boolean isMandatory;
    protected EnumType attributeEnums;
    protected EnumValues attributeStatus;
    protected EnumValues attributeValueSource;
    protected EnumValues attributeValueKey;

    /**
     * Gets the value of the attributeId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAttributeId() {
        return attributeId;
    }

    /**
     * Sets the value of the attributeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAttributeId(Integer value) {
        this.attributeId = value;
    }

    /**
     * Gets the value of the attributeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * Sets the value of the attributeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeName(String value) {
        this.attributeName = value;
    }

    /**
     * Gets the value of the attributeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeCode() {
        return attributeCode;
    }

    /**
     * Sets the value of the attributeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeCode(String value) {
        this.attributeCode = value;
    }

    /**
     * Gets the value of the attributeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeType() {
        return attributeType;
    }

    /**
     * Sets the value of the attributeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeType(String value) {
        this.attributeType = value;
    }

    /**
     * Gets the value of the attributeLength property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAttributeLength() {
        return attributeLength;
    }

    /**
     * Sets the value of the attributeLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAttributeLength(Integer value) {
        this.attributeLength = value;
    }

    /**
     * Gets the value of the attributeDefaultValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeDefaultValue() {
        return attributeDefaultValue;
    }

    /**
     * Sets the value of the attributeDefaultValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeDefaultValue(String value) {
        this.attributeDefaultValue = value;
    }

    /**
     * Gets the value of the attributeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeDescription() {
        return attributeDescription;
    }

    /**
     * Sets the value of the attributeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeDescription(String value) {
        this.attributeDescription = value;
    }

    /**
     * Gets the value of the isMandatory property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsMandatory() {
        return isMandatory;
    }

    /**
     * Sets the value of the isMandatory property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsMandatory(Boolean value) {
        this.isMandatory = value;
    }

    /**
     * Gets the value of the attributeEnums property.
     * 
     * @return
     *     possible object is
     *     {@link EnumType }
     *     
     */
    public EnumType getAttributeEnums() {
        return attributeEnums;
    }

    /**
     * Sets the value of the attributeEnums property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumType }
     *     
     */
    public void setAttributeEnums(EnumType value) {
        this.attributeEnums = value;
    }

    /**
     * Gets the value of the attributeStatus property.
     * 
     * @return
     *     possible object is
     *     {@link EnumValues }
     *     
     */
    public EnumValues getAttributeStatus() {
        return attributeStatus;
    }

    /**
     * Sets the value of the attributeStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumValues }
     *     
     */
    public void setAttributeStatus(EnumValues value) {
        this.attributeStatus = value;
    }

    /**
     * Gets the value of the attributeValueSource property.
     * 
     * @return
     *     possible object is
     *     {@link EnumValues }
     *     
     */
    public EnumValues getAttributeValueSource() {
        return attributeValueSource;
    }

    /**
     * Sets the value of the attributeValueSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumValues }
     *     
     */
    public void setAttributeValueSource(EnumValues value) {
        this.attributeValueSource = value;
    }

    /**
     * Gets the value of the attributeValueKey property.
     * 
     * @return
     *     possible object is
     *     {@link EnumValues }
     *     
     */
    public EnumValues getAttributeValueKey() {
        return attributeValueKey;
    }

    /**
     * Sets the value of the attributeValueKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumValues }
     *     
     */
    public void setAttributeValueKey(EnumValues value) {
        this.attributeValueKey = value;
    }

}
