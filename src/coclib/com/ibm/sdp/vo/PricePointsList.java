
package coclib.com.ibm.sdp.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PricePointsList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PricePointsList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pricePoints" type="{http://CocLib/com/ibm/sdp/vo}PricePoint" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PricePointsList", propOrder = {
    "pricePoints"
})
public class PricePointsList {

    protected List<PricePoint> pricePoints;

    /**
     * Gets the value of the pricePoints property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pricePoints property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPricePoints().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PricePoint }
     * 
     * 
     */
    public List<PricePoint> getPricePoints() {
        if (pricePoints == null) {
            pricePoints = new ArrayList<PricePoint>();
        }
        return this.pricePoints;
    }

}
