
package coclib.com.ibm.sdp.vo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the coclib.com.ibm.sdp.vo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: coclib.com.ibm.sdp.vo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ServiceException }
     * 
     */
    public ServiceException createServiceException() {
        return new ServiceException();
    }

    /**
     * Create an instance of {@link CDRConfigurationList }
     * 
     */
    public CDRConfigurationList createCDRConfigurationList() {
        return new CDRConfigurationList();
    }

    /**
     * Create an instance of {@link AuditModules }
     * 
     */
    public AuditModules createAuditModules() {
        return new AuditModules();
    }

    /**
     * Create an instance of {@link CPServiceDetails }
     * 
     */
    public CPServiceDetails createCPServiceDetails() {
        return new CPServiceDetails();
    }

    /**
     * Create an instance of {@link ContentProvider }
     * 
     */
    public ContentProvider createContentProvider() {
        return new ContentProvider();
    }

    /**
     * Create an instance of {@link UsageDataRecord }
     * 
     */
    public UsageDataRecord createUsageDataRecord() {
        return new UsageDataRecord();
    }

    /**
     * Create an instance of {@link MessageBo }
     * 
     */
    public MessageBo createMessageBo() {
        return new MessageBo();
    }

    /**
     * Create an instance of {@link Attachment }
     * 
     */
    public Attachment createAttachment() {
        return new Attachment();
    }

    /**
     * Create an instance of {@link AttributeDetailsList }
     * 
     */
    public AttributeDetailsList createAttributeDetailsList() {
        return new AttributeDetailsList();
    }

    /**
     * Create an instance of {@link ErrorDetails }
     * 
     */
    public ErrorDetails createErrorDetails() {
        return new ErrorDetails();
    }

    /**
     * Create an instance of {@link CPService }
     * 
     */
    public CPService createCPService() {
        return new CPService();
    }

    /**
     * Create an instance of {@link EnumValues }
     * 
     */
    public EnumValues createEnumValues() {
        return new EnumValues();
    }

    /**
     * Create an instance of {@link AuditLog }
     * 
     */
    public AuditLog createAuditLog() {
        return new AuditLog();
    }

    /**
     * Create an instance of {@link ContentProviderList }
     * 
     */
    public ContentProviderList createContentProviderList() {
        return new ContentProviderList();
    }

    /**
     * Create an instance of {@link ApplicationDetailsList }
     * 
     */
    public ApplicationDetailsList createApplicationDetailsList() {
        return new ApplicationDetailsList();
    }

    /**
     * Create an instance of {@link AttributeDetails }
     * 
     */
    public AttributeDetails createAttributeDetails() {
        return new AttributeDetails();
    }

    /**
     * Create an instance of {@link EnumType }
     * 
     */
    public EnumType createEnumType() {
        return new EnumType();
    }

    /**
     * Create an instance of {@link PurchaseInput }
     * 
     */
    public PurchaseInput createPurchaseInput() {
        return new PurchaseInput();
    }

    /**
     * Create an instance of {@link CacheKeys }
     * 
     */
    public CacheKeys createCacheKeys() {
        return new CacheKeys();
    }

    /**
     * Create an instance of {@link PriceAndMessaging }
     * 
     */
    public PriceAndMessaging createPriceAndMessaging() {
        return new PriceAndMessaging();
    }

    /**
     * Create an instance of {@link ApplicationDetails }
     * 
     */
    public ApplicationDetails createApplicationDetails() {
        return new ApplicationDetails();
    }

    /**
     * Create an instance of {@link AllPurchaseInput }
     * 
     */
    public AllPurchaseInput createAllPurchaseInput() {
        return new AllPurchaseInput();
    }

    /**
     * Create an instance of {@link ApplicationProvider }
     * 
     */
    public ApplicationProvider createApplicationProvider() {
        return new ApplicationProvider();
    }

    /**
     * Create an instance of {@link PricePoint }
     * 
     */
    public PricePoint createPricePoint() {
        return new PricePoint();
    }

    /**
     * Create an instance of {@link UpdateObject }
     * 
     */
    public UpdateObject createUpdateObject() {
        return new UpdateObject();
    }

    /**
     * Create an instance of {@link PurchaseByDateInput }
     * 
     */
    public PurchaseByDateInput createPurchaseByDateInput() {
        return new PurchaseByDateInput();
    }

    /**
     * Create an instance of {@link MessageTemplate }
     * 
     */
    public MessageTemplate createMessageTemplate() {
        return new MessageTemplate();
    }

    /**
     * Create an instance of {@link MessageTemplateList }
     * 
     */
    public MessageTemplateList createMessageTemplateList() {
        return new MessageTemplateList();
    }

    /**
     * Create an instance of {@link UsageDataRecordList }
     * 
     */
    public UsageDataRecordList createUsageDataRecordList() {
        return new UsageDataRecordList();
    }

    /**
     * Create an instance of {@link AuditEntity }
     * 
     */
    public AuditEntity createAuditEntity() {
        return new AuditEntity();
    }

    /**
     * Create an instance of {@link AuditAction }
     * 
     */
    public AuditAction createAuditAction() {
        return new AuditAction();
    }

    /**
     * Create an instance of {@link PurchaseForSubscriberInput }
     * 
     */
    public PurchaseForSubscriberInput createPurchaseForSubscriberInput() {
        return new PurchaseForSubscriberInput();
    }

    /**
     * Create an instance of {@link CDRAttribute }
     * 
     */
    public CDRAttribute createCDRAttribute() {
        return new CDRAttribute();
    }

    /**
     * Create an instance of {@link CDRConfiguration }
     * 
     */
    public CDRConfiguration createCDRConfiguration() {
        return new CDRConfiguration();
    }

    /**
     * Create an instance of {@link MessageTemplateDetails }
     * 
     */
    public MessageTemplateDetails createMessageTemplateDetails() {
        return new MessageTemplateDetails();
    }

    /**
     * Create an instance of {@link ResponseObject }
     * 
     */
    public ResponseObject createResponseObject() {
        return new ResponseObject();
    }

    /**
     * Create an instance of {@link PricePointsList }
     * 
     */
    public PricePointsList createPricePointsList() {
        return new PricePointsList();
    }

    /**
     * Create an instance of {@link AuditLogList }
     * 
     */
    public AuditLogList createAuditLogList() {
        return new AuditLogList();
    }

    /**
     * Create an instance of {@link PurchaseForXDaysInput }
     * 
     */
    public PurchaseForXDaysInput createPurchaseForXDaysInput() {
        return new PurchaseForXDaysInput();
    }

    /**
     * Create an instance of {@link MessageList }
     * 
     */
    public MessageList createMessageList() {
        return new MessageList();
    }

}
