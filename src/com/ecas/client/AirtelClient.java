package com.ecas.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.MessageContext;

import chargingprocess.com.ibm.sdp.services.charging.abstraction.charging.binding.Charging;
import chargingprocess.com.ibm.sdp.services.charging.abstraction.charging.binding.ChargingExport1ChargingHttpService;
import chargingprocess.com.ibm.sdp.services.charging.vo.ChargingRequest;
import chargingprocess.com.ibm.sdp.services.charging.vo.ChargingResponse;

import com.ecas.wshandler.SOAPLoggingHandler;

public class AirtelClient {
	
	
	private Charging port = null;
	
	
	public AirtelClient(File keystore,String keystorePassword,String endpoint,String username,String password) throws Exception {
		this(new FileInputStream(keystore),keystorePassword,endpoint,username,password);
	}
	
	public AirtelClient(InputStream keystoreStream,String keystorePassword,String endpoint,String username,String password) throws Exception {
		//Bypass HostnameVerifier
		HostnameVerifier hv = new HostnameVerifier(){
			public boolean verify(String urlHostName, SSLSession session) {		
			return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(hv);
		
		//Create SSL
		KeyStore ks = KeyStore.getInstance("JKS");
		ks.load(keystoreStream, keystorePassword.toCharArray());// Location of Key Store c:\\SDPKeystore.dat,12345678
		
		KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
		kmf.init(ks, keystorePassword.toCharArray());
		
		TrustManager[] tm;
		TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
		tmf.init(ks);
		tm = tmf.getTrustManagers();
		SSLContext sslContext = SSLContext.getInstance("SSL");
		sslContext.init(kmf.getKeyManagers(), tm, null);
		
		
		//Add authorization header
		String usernamePassword = username + ":" + password; // Username and password will be provided by TWSS Admin
		String encoding = null;
		Base64.Encoder encoder = Base64.getEncoder();
		encoding = new String(encoder.encode( usernamePassword.getBytes() )); 
		Map requestHeaders = new HashMap();
		List<String>  lst= new ArrayList<String>();
		lst.add( "Basic " + encoding);
        requestHeaders.put("Authorization", lst);
		
		ChargingExport1ChargingHttpService service = new ChargingExport1ChargingHttpService();
		port = service.getChargingHttpServiceChargingHttpPort();
		 
		BindingProvider prov = (BindingProvider)port;
		//add log
//		List<Handler> handlerChain = prov.getBinding().getHandlerChain();
//		handlerChain.add(new SOAPLoggingHandler());
//		prov.getBinding().setHandlerChain(handlerChain);
		prov.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint); 
		prov.getRequestContext().put("com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory", sslContext.getSocketFactory());	
        prov.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, requestHeaders);
	}
	
	public ChargingResponse charge(ChargingRequest request) throws Exception {					
		ChargingResponse response = port.charge(request);
		return response;
		
	}
	
	
}
