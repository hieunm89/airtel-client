
package chargingprocess.com.ibm.sdp.services.charging.abstraction.charging;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import chargingprocess.com.ibm.sdp.services.charging.vo.ChargingRequest;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputMsg" type="{http://ChargingProcess/com/ibm/sdp/services/charging/vo}ChargingRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputMsg"
})
@XmlRootElement(name = "charge")
public class Charge {

    @XmlElement(required = true, nillable = true)
    protected ChargingRequest inputMsg;

    /**
     * Gets the value of the inputMsg property.
     * 
     * @return
     *     possible object is
     *     {@link ChargingRequest }
     *     
     */
    public ChargingRequest getInputMsg() {
        return inputMsg;
    }

    /**
     * Sets the value of the inputMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChargingRequest }
     *     
     */
    public void setInputMsg(ChargingRequest value) {
        this.inputMsg = value;
    }

}
