
package chargingprocess.com.ibm.sdp.services.charging.abstraction.charging;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import chargingprocess.com.ibm.sdp.services.charging.vo.ChargingResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="outputMsg" type="{http://ChargingProcess/com/ibm/sdp/services/charging/vo}ChargingResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "outputMsg"
})
@XmlRootElement(name = "chargeResponse")
public class ChargeResponse {

    @XmlElement(required = true, nillable = true)
    protected ChargingResponse outputMsg;

    /**
     * Gets the value of the outputMsg property.
     * 
     * @return
     *     possible object is
     *     {@link ChargingResponse }
     *     
     */
    public ChargingResponse getOutputMsg() {
        return outputMsg;
    }

    /**
     * Sets the value of the outputMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChargingResponse }
     *     
     */
    public void setOutputMsg(ChargingResponse value) {
        this.outputMsg = value;
    }

}
