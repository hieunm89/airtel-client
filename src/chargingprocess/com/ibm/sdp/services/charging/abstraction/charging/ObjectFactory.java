
package chargingprocess.com.ibm.sdp.services.charging.abstraction.charging;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import coclib.com.ibm.sdp.vo.ServiceException;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the chargingprocess.com.ibm.sdp.services.charging.abstraction.charging package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ChargeServiceException_QNAME = new QName("http://ChargingProcess/com/ibm/sdp/services/charging/abstraction/Charging", "charge_serviceException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: chargingprocess.com.ibm.sdp.services.charging.abstraction.charging
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ChargeResponse }
     * 
     */
    public ChargeResponse createChargeResponse() {
        return new ChargeResponse();
    }

    /**
     * Create an instance of {@link Charge }
     * 
     */
    public Charge createCharge() {
        return new Charge();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ChargingProcess/com/ibm/sdp/services/charging/abstraction/Charging", name = "charge_serviceException")
    public JAXBElement<ServiceException> createChargeServiceException(ServiceException value) {
        return new JAXBElement<ServiceException>(_ChargeServiceException_QNAME, ServiceException.class, null, value);
    }

}
