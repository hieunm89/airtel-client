
package chargingprocess.com.ibm.sdp.services.charging.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChargingRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChargingRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operation" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="debit"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contentId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="itemName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contentDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="circleId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lineOfBusiness" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerSegment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contentMediaType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serviceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="actualPrice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="basePrice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="discountApplied" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paymentMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="revenuePercent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="netShare" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="customerClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Content Purchase"/>
 *               &lt;enumeration value="CSR Move To Purchased"/>
 *               &lt;enumeration value="CSR Purchase"/>
 *               &lt;enumeration value="CSR Subscription Purchase"/>
 *               &lt;enumeration value="Event Based Purchase"/>
 *               &lt;enumeration value="Gifting"/>
 *               &lt;enumeration value="Licence Renewal"/>
 *               &lt;enumeration value="Purchase Refund"/>
 *               &lt;enumeration value="Subscription Purchase"/>
 *               &lt;enumeration value="ReSubscription"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="localTimeStamp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subscriptionTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subscriptionName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="parentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deliveryChannel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subscriptionExternalId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contentSize" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="copyrightId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sMSkeyword" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="srcCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contentUrl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subscriptiondays" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="copyrightDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChargingRequest", propOrder = {
    "operation",
    "userId",
    "contentId",
    "itemName",
    "contentDescription",
    "circleId",
    "lineOfBusiness",
    "customerSegment",
    "contentMediaType",
    "serviceId",
    "parentId",
    "actualPrice",
    "basePrice",
    "discountApplied",
    "paymentMethod",
    "revenuePercent",
    "netShare",
    "cpId",
    "customerClass",
    "eventType",
    "localTimeStamp",
    "transactionId",
    "subscriptionTypeCode",
    "subscriptionName",
    "parentType",
    "deliveryChannel",
    "subscriptionExternalId",
    "contentSize",
    "currency",
    "copyrightId",
    "smSkeyword",
    "srcCode",
    "contentUrl",
    "subscriptiondays",
    "cpTransactionId",
    "copyrightDescription"
})
public class ChargingRequest {

    protected String operation;
    @XmlElement(required = true)
    protected String userId;
    @XmlElement(required = true)
    protected String contentId;
    @XmlElement(required = true)
    protected String itemName;
    @XmlElement(required = true)
    protected String contentDescription;
    protected String circleId;
    protected String lineOfBusiness;
    protected String customerSegment;
    @XmlElement(required = true)
    protected String contentMediaType;
    protected String serviceId;
    protected String parentId;
    @XmlElement(required = true)
    protected String actualPrice;
    @XmlElement(required = true)
    protected String basePrice;
    @XmlElement(required = true)
    protected String discountApplied;
    protected String paymentMethod;
    protected String revenuePercent;
    protected String netShare;
    @XmlElement(required = true)
    protected String cpId;
    protected String customerClass;
    protected String eventType;
    protected String localTimeStamp;
    protected String transactionId;
    @XmlElement(required = true)
    protected String subscriptionTypeCode;
    @XmlElement(required = true)
    protected String subscriptionName;
    protected String parentType;
    @XmlElement(required = true)
    protected String deliveryChannel;
    @XmlElement(required = true)
    protected String subscriptionExternalId;
    protected String contentSize;
    @XmlElement(required = true)
    protected String currency;
    @XmlElement(required = true)
    protected String copyrightId;
    @XmlElement(name = "sMSkeyword", required = true)
    protected String smSkeyword;
    @XmlElement(required = true)
    protected String srcCode;
    @XmlElement(required = true)
    protected String contentUrl;
    protected String subscriptiondays;
    protected String cpTransactionId;
    protected String copyrightDescription;

    /**
     * Gets the value of the operation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperation() {
        return operation;
    }

    /**
     * Sets the value of the operation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperation(String value) {
        this.operation = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the contentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * Sets the value of the contentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentId(String value) {
        this.contentId = value;
    }

    /**
     * Gets the value of the itemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * Sets the value of the itemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemName(String value) {
        this.itemName = value;
    }

    /**
     * Gets the value of the contentDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentDescription() {
        return contentDescription;
    }

    /**
     * Sets the value of the contentDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentDescription(String value) {
        this.contentDescription = value;
    }

    /**
     * Gets the value of the circleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCircleId() {
        return circleId;
    }

    /**
     * Sets the value of the circleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCircleId(String value) {
        this.circleId = value;
    }

    /**
     * Gets the value of the lineOfBusiness property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineOfBusiness() {
        return lineOfBusiness;
    }

    /**
     * Sets the value of the lineOfBusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineOfBusiness(String value) {
        this.lineOfBusiness = value;
    }

    /**
     * Gets the value of the customerSegment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerSegment() {
        return customerSegment;
    }

    /**
     * Sets the value of the customerSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerSegment(String value) {
        this.customerSegment = value;
    }

    /**
     * Gets the value of the contentMediaType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentMediaType() {
        return contentMediaType;
    }

    /**
     * Sets the value of the contentMediaType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentMediaType(String value) {
        this.contentMediaType = value;
    }

    /**
     * Gets the value of the serviceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceId() {
        return serviceId;
    }

    /**
     * Sets the value of the serviceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceId(String value) {
        this.serviceId = value;
    }

    /**
     * Gets the value of the parentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * Sets the value of the parentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentId(String value) {
        this.parentId = value;
    }

    /**
     * Gets the value of the actualPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActualPrice() {
        return actualPrice;
    }

    /**
     * Sets the value of the actualPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActualPrice(String value) {
        this.actualPrice = value;
    }

    /**
     * Gets the value of the basePrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasePrice() {
        return basePrice;
    }

    /**
     * Sets the value of the basePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasePrice(String value) {
        this.basePrice = value;
    }

    /**
     * Gets the value of the discountApplied property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountApplied() {
        return discountApplied;
    }

    /**
     * Sets the value of the discountApplied property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountApplied(String value) {
        this.discountApplied = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the revenuePercent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRevenuePercent() {
        return revenuePercent;
    }

    /**
     * Sets the value of the revenuePercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRevenuePercent(String value) {
        this.revenuePercent = value;
    }

    /**
     * Gets the value of the netShare property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetShare() {
        return netShare;
    }

    /**
     * Sets the value of the netShare property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetShare(String value) {
        this.netShare = value;
    }

    /**
     * Gets the value of the cpId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpId() {
        return cpId;
    }

    /**
     * Sets the value of the cpId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpId(String value) {
        this.cpId = value;
    }

    /**
     * Gets the value of the customerClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerClass() {
        return customerClass;
    }

    /**
     * Sets the value of the customerClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerClass(String value) {
        this.customerClass = value;
    }

    /**
     * Gets the value of the eventType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * Sets the value of the eventType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventType(String value) {
        this.eventType = value;
    }

    /**
     * Gets the value of the localTimeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalTimeStamp() {
        return localTimeStamp;
    }

    /**
     * Sets the value of the localTimeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalTimeStamp(String value) {
        this.localTimeStamp = value;
    }

    /**
     * Gets the value of the transactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the subscriptionTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriptionTypeCode() {
        return subscriptionTypeCode;
    }

    /**
     * Sets the value of the subscriptionTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriptionTypeCode(String value) {
        this.subscriptionTypeCode = value;
    }

    /**
     * Gets the value of the subscriptionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriptionName() {
        return subscriptionName;
    }

    /**
     * Sets the value of the subscriptionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriptionName(String value) {
        this.subscriptionName = value;
    }

    /**
     * Gets the value of the parentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentType() {
        return parentType;
    }

    /**
     * Sets the value of the parentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentType(String value) {
        this.parentType = value;
    }

    /**
     * Gets the value of the deliveryChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryChannel() {
        return deliveryChannel;
    }

    /**
     * Sets the value of the deliveryChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryChannel(String value) {
        this.deliveryChannel = value;
    }

    /**
     * Gets the value of the subscriptionExternalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriptionExternalId() {
        return subscriptionExternalId;
    }

    /**
     * Sets the value of the subscriptionExternalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriptionExternalId(String value) {
        this.subscriptionExternalId = value;
    }

    /**
     * Gets the value of the contentSize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentSize() {
        return contentSize;
    }

    /**
     * Sets the value of the contentSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentSize(String value) {
        this.contentSize = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the copyrightId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCopyrightId() {
        return copyrightId;
    }

    /**
     * Sets the value of the copyrightId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCopyrightId(String value) {
        this.copyrightId = value;
    }

    /**
     * Gets the value of the smSkeyword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSMSkeyword() {
        return smSkeyword;
    }

    /**
     * Sets the value of the smSkeyword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSMSkeyword(String value) {
        this.smSkeyword = value;
    }

    /**
     * Gets the value of the srcCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrcCode() {
        return srcCode;
    }

    /**
     * Sets the value of the srcCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrcCode(String value) {
        this.srcCode = value;
    }

    /**
     * Gets the value of the contentUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentUrl() {
        return contentUrl;
    }

    /**
     * Sets the value of the contentUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentUrl(String value) {
        this.contentUrl = value;
    }

    /**
     * Gets the value of the subscriptiondays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriptiondays() {
        return subscriptiondays;
    }

    /**
     * Sets the value of the subscriptiondays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriptiondays(String value) {
        this.subscriptiondays = value;
    }

    /**
     * Gets the value of the cpTransactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpTransactionId() {
        return cpTransactionId;
    }

    /**
     * Sets the value of the cpTransactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpTransactionId(String value) {
        this.cpTransactionId = value;
    }

    /**
     * Gets the value of the copyrightDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCopyrightDescription() {
        return copyrightDescription;
    }

    /**
     * Sets the value of the copyrightDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCopyrightDescription(String value) {
        this.copyrightDescription = value;
    }

}
