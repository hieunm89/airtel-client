
package chargingprocess.com.ibm.sdp.services.charging.vo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the chargingprocess.com.ibm.sdp.services.charging.vo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: chargingprocess.com.ibm.sdp.services.charging.vo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ChargingResponse }
     * 
     */
    public ChargingResponse createChargingResponse() {
        return new ChargingResponse();
    }

    /**
     * Create an instance of {@link ChargingRequest }
     * 
     */
    public ChargingRequest createChargingRequest() {
        return new ChargingRequest();
    }

    /**
     * Create an instance of {@link SubscriberInformation }
     * 
     */
    public SubscriberInformation createSubscriberInformation() {
        return new SubscriberInformation();
    }

}
