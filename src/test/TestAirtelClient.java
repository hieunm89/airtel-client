package test;

import chargingprocess.com.ibm.sdp.services.charging.vo.ChargingRequest;
import chargingprocess.com.ibm.sdp.services.charging.vo.ChargingResponse;

import com.ecas.client.AirtelClient;

public class TestAirtelClient {
	public static void main(String args[]){
		try{
			AirtelClient client  = new AirtelClient(TestAirtelClient.class.getResourceAsStream("/certificate/client.truststore"),"123456",
					"https://196.46.244.21:8443/ChargingServiceFlowWeb/sca/ChargingExport1",
					"2323244_GREENBEE_NG","iM4yXN9V");
			ChargingRequest request = new ChargingRequest();
			request.setOperation("debit");
			request.setUserId("2347012150912");
			request.setContentId("Test");
			request.setItemName("Waffi - 2017-04-16 12:00:00");
			request.setContentDescription("Waffi - 2017-04-16 12:00:00");
			request.setContentMediaType("SMSSelfDevelopmentServices");
			request.setServiceId("1744");
			request.setActualPrice("5");
			request.setBasePrice("0");
			request.setDiscountApplied("0");
			request.setNetShare("0");
			request.setCpId("2323244_GREENBEE_NG");
			request.setEventType("Subscription Purchase");
			request.setSubscriptionTypeCode("38661");
			request.setSubscriptionName("46929");
			request.setDeliveryChannel("SMS");
			request.setSubscriptionExternalId("2");
			request.setCurrency("NGN");
			request.setCopyrightId("Test");
			request.setCpTransactionId("14923404000001");
			request.setCopyrightDescription("copyright");
			request.setSMSkeyword("38661");
			request.setSrcCode("38661");
			request.setContentUrl("http://ecas.ng");
			request.setSubscriptiondays("1");
			
			
			ChargingResponse response = client.charge(request);
			System.out.println("TransactionId:"+response.getTransactionId());
			System.out.println("CpTransactionId:"+response.getCpTransactionId());
			System.out.println("Status:"+response.getStatus());
			System.out.println("ErrorCode:"+response.getError().getErrorCode());
			System.out.println("ErrorMessage:"+response.getError().getErrorMessage());
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
